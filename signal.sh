#!/bin/bash

# notify-send "Self Control Panel Signal."
# kill -s 10 $(pidof SelfCtrlPanel)
# kill -s 10 $(pidof whatAmIDoing)
kill -s 10 $(pidof "What am I Doing?")
# kill -s 10 $(pidof what-am-i-doing)
# kill -s 10 $(ps -C SelfCtrlPanel)

# Make it so it doesn't open in background on Linux Mint
# since wmctrl may not work or be installed everywhere, we skip this part if it isn't present
# if wmctrl isn't installed and you want it, try: sudo apt install wmctrl
if hash wmctrl 2>/dev/null; then
        sleep 0.1s # because window doesn't exist yet...
        # wmctrl -a "Self Control Panel" # raise window by name. 
        wmctrl -a "What Am I Doing?" # raise window by name. 
    else
        echo "wmctrl is not installed. skipping..."
fi


# TODO: use something like this to pass in new task /task search string
# word=`xsel`
# set -o errexit
# echo -n "$word " >> $HOME/spelling_study_words.txt